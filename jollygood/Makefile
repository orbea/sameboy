SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

include $(SOURCEDIR)/../version.mk
export VERSION

CC ?= cc
CC_FOR_BUILD ?= $(CC)
CFLAGS ?= -O2
FLAGS := -std=gnu11
FLAGS_PB12 := -Os -std=c99
INCLUDES := -I$(SOURCEDIR)/..
DEFS := -D_GNU_SOURCE -DGB_INTERNAL -DGB_DISABLE_REWIND -DGB_DISABLE_DEBUGGER \
	-DGB_VERSION=\"$(VERSION)\"

PKG_CONFIG ?= pkg-config
CFLAGS_JG := $(shell $(PKG_CONFIG) --cflags jg)

WARNINGS := -Wall -Wno-strict-aliasing -Wno-multichar -Wno-unused-result
WARNINGS_PB12 := -Wall -Wextra -Wshadow
PIC := -fPIC
SHARED := $(PIC)

NAME := sameboy
PREFIX ?= /usr/local
LIBDIR ?= $(PREFIX)/lib
DATAROOTDIR ?= $(PREFIX)/share
DATADIR ?= $(DATAROOTDIR)
DOCDIR ?= $(DATAROOTDIR)/doc/$(NAME)

UNAME := $(shell uname -s)
ifeq ($(UNAME), Darwin)
	SHARED += -dynamiclib
	TARGET := $(NAME).dylib
else ifeq ($(OS), Windows_NT)
	SHARED += -shared
	TARGET := $(NAME).dll
else
	SHARED += -shared
	TARGET := $(NAME).so
endif

CSRCS := Core/apu.c \
	Core/camera.c \
	Core/cheats.c \
	Core/display.c \
	Core/gb.c \
	Core/joypad.c \
	Core/mbc.c \
	Core/memory.c \
	Core/random.c \
	Core/rumble.c \
	Core/save_state.c \
	Core/sgb.c \
	Core/sm83_cpu.c \
	Core/symbol_hash.c \
	Core/timing.c \
	jg.c

BOOTROMS := $(NAME)/agb_boot.bin \
	$(NAME)/cgb_boot.bin \
	$(NAME)/cgb_boot_fast.bin \
	$(NAME)/dmg_boot.bin \
	$(NAME)/mgb_boot.bin \
	$(NAME)/sgb_boot.bin \
	$(NAME)/sgb2_boot.bin

PB12_COMPRESS := pb12
PB12_BOOTLOGO := SameBoyLogo.pb12
2BPP_BOOTLOGO := SameBoyLogo.2bpp

# Object dirs
MKDIRS := Core

OBJDIR := objs

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o))

# Compiler command
COMPILE = $(strip $(1) $(CPPFLAGS) $(PIC) $(2) -c $< -o $@)
COMPILE_C = $(call COMPILE, $(CC) $(CFLAGS), $(1))
COMPILE_C_BUILD = $(strip $(CC_FOR_BUILD) $(1) $< -o $@)

# Info command
COMPILE_INFO = $(info $(subst $(SOURCEDIR)/,,$(1)))

# BootROM command
BUILD_PB12 = $(call COMPILE_C_BUILD, $(FLAGS_PB12) $(WARNINGS_PB12))

# Core commands
BUILD_JG = $(call COMPILE_C, $(FLAGS) $(WARNINGS) $(DEFS) $(INCLUDES) \
	$(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_C, $(FLAGS) $(WARNINGS) $(DEFS) $(INCLUDES))

.PHONY: all bootroms clean install install-strip uninstall

all: $(NAME)/$(TARGET) bootroms

$(OBJDIR)/Core/%.o: $(SOURCEDIR)/../Core/%.c $(OBJDIR)/.tag
	$(call COMPILE_INFO, $(BUILD_MAIN))
	@$(BUILD_MAIN)

$(OBJDIR)/%.o: $(SOURCEDIR)/%.c $(OBJDIR)/.tag
	$(call COMPILE_INFO, $(BUILD_JG))
	@$(BUILD_JG)

$(NAME)/%.bin: $(SOURCEDIR)/../BootROMs/%.asm $(PB12_BOOTLOGO) $(OBJDIR)/.tag
	@rgbasm -i $(SOURCEDIR)/../BootROMs/ -i $(NAME) -o $@.tmp $<
	@rgblink -o $@.tmp2 $@.tmp
	dd if=$@.tmp2 of=$@ count=1 bs=$(if $(findstring dmg,$@)$(findstring sgb,$@),256,2304)
	@rm $@.tmp $@.tmp2

$(OBJDIR)/.tag:
	@mkdir -p -- $(patsubst %,$(OBJDIR)/%,$(MKDIRS)) $(NAME)
	@touch $@

$(NAME)/$(TARGET): $(OBJS)
	$(CC) $^ $(LDFLAGS) $(SHARED) -o $@

$(PB12_COMPRESS): $(SOURCEDIR)/../BootROMs/pb12.c
	$(call COMPILE_INFO, $(BUILD_PB12))
	@$(BUILD_PB12)

$(2BPP_BOOTLOGO): $(SOURCEDIR)/../BootROMs/SameBoyLogo.png
	@rgbgfx -Z -u -c embedded -o $@ $<

$(PB12_BOOTLOGO): $(2BPP_BOOTLOGO) $(PB12_COMPRESS)
	./$(PB12_COMPRESS) < $< > $@

bootroms: $(BOOTROMS)

clean:
	rm -rf $(OBJDIR) $(NAME) $(PB12_COMPRESS) $(2BPP_BOOTLOGO) $(PB12_BOOTLOGO)

install: all
	@mkdir -p $(DESTDIR)$(DOCDIR)
	@mkdir -p $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	@mkdir -p $(DESTDIR)$(LIBDIR)/jollygood
	cp $(NAME)/$(TARGET) $(DESTDIR)$(LIBDIR)/jollygood/
	cp $(NAME)/agb_boot.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/cgb_boot.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/cgb_boot_fast.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/dmg_boot.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/mgb_boot.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/sgb_boot.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/sgb2_boot.bin $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(SOURCEDIR)/README $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/../LICENSE $(DESTDIR)$(DOCDIR)

install-strip: install
	strip $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)

uninstall:
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -rf $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	rm -f $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)
